step-cert
=========

Install step-cli and issue x509 certificate from Step-CA via Json Web Key (JWK).

Every inventory host will get it's own certificate.

A systemd renewal service called `step-renew.service` will be created to keep the issued certificate up to date.<br>

The step daemon will attempt a renewal when the certificate's lifetime is approximately two-thirds elapsed.<br> 
Example: A certificate with 24 hour lifetime will be renewed after about 16 hours.


Requirements
------------

- Existing Step-CA server
- Configured JWK provisioner that is allowed to issue certificates

Role Variables
--------------

The default values for the variables are set in `defaults/main.yml`:
```yml
# Step CI download URLs
step_cli_download_base_url: "https://github.com/smallstep/cli/releases/download"
step_cli_download_local_url: "https://files.repository.mueller.de/step-ca"

# Use latest as value to fetch the latest release from github (firewall access required) or provide specific version
step_cli_version: "0.23.2"

# Step CLI installation and config path
step_cli_bin: /usr/bin/step
step_config_dir: /etc/step

# Step CLI bootstrap config
step_ca_url: https://stepca.test.2ln.mueller.de
step_ca_root_fingerprint: b43da897e854ca676e602519cebeba10667af11dc81525ce257d9823e268f4d2

# JWK provisioner name and key to issue certificates
step_ca_provisioner: "step" 
step_ca_provisioner_jwk: "" # Required JWK authentication

# Certificate information
step_cert_common_name: "{{ ansible_fqdn }}"

# ----------------------------------------
# Optional variables
# ----------------------------------------

#step_cert_subject_alt_names: 
#  - app.example.com
#  - app-01.example.com
#  - app-02.example.com
#  - app-03.example.com

#step_cert_duration: 24h # Default is maximum duration of Step-CA provisioner configuration. Can be overwritten to shorter lifetime

#step_cert_renewal_reload_services:
#  - nginx.service
```

**Reload/Restart Services after renewal:**

You can provide services that should be reloaded or restarted by `step-renew.service` after it has renewed the certificate:
```yml
step_cert_renewal_reload_services:
  - nginx.service
```
<br>

Dependencies
------------
- No Dependencies

Example Inventory
------------

```yml
all:
  children:
    app:
      hosts:
        app-01.example.com:
          ansible_host: 10.199.34.146
        app-02.example.com:
          ansible_host: 10.199.34.147
        app-03.example.com:
          ansible_host: 10.199.34.148
      vars:
        step_cert_subject_alt_names: 
          - app.example.com
          - app-01.example.com
          - app-02.example.com
          - app-03.example.com
```

Example Playbook
----------------

```yml
- hosts: all
  become: yes
  gather_facts: yes
  roles:
    - role: step-cert
      step_ca_provisioner_jwk: "SecretFromVault"

```

License
-------

GPLv3

Author Information
------------------

Oleg Franko